// https://developer.ibm.com/tutorials/l-openssl/

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include <openssl/x509v3.h>
#include "tls_common.h"

static void help(void){
	printf("\
 -n --chain <file>       public certificate chain for upstream certs beyond server \n\
 -t --host <host>:<port> host and port to connect to\n\
 -h --help               this help\n\
");
}

const char* chain, *connection_host_str;
char servername[256];

/** @brief if h is a <host>:<port>, strip off the colon onwards
 * Returns host in `servername` global
 */
static void host_parse(const char* h){
	char* colon;
	strncpy(servername, h, sizeof servername-1);
	if ( (colon = strstr(servername, ":")))
		*colon=0;
}
static int options(int argc, char **argv){
	int err = 0, c;
	while (1){
		static struct option long_options[] ={
			{"chain",  required_argument, 0, 'n'},
			{"host",  required_argument, 0, 't'},
			{"help",  no_argument, 0, 'h'},
			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;
		
		if ( (c = getopt_long (argc, argv, "n:t:",
			long_options, &option_index)) == -1)
			break;
		switch (c){
			case 'n':
				chain = optarg;
				break;
			case 't':
				connection_host_str = optarg;
				host_parse(optarg);
				break;
			case 'h':
				help();
				exit(EXIT_SUCCESS);
				break;
			default:
				printf("c=%c(%d)\n", c, c);
				err = 1;
		}
	}
	return err;
}

int main(int argc, char** argv){
	char tmpbuf[1024];
	int len;
	BIO * bio, *out;
	int r;
	SSL_CTX*  ctx=0;
	SSL*  ssl=0;
	X509_VERIFY_PARAM *param = NULL;

	if (options(argc, argv)){
		printf("Err.\n");
		help();
		exit(EXIT_FAILURE);
	}
	
	trap_signals();

	SSL_load_error_strings(); // returns void
	ERR_load_BIO_strings(); // returns void
	OpenSSL_add_all_algorithms(); // returns void

	if (!(ctx = SSL_CTX_new(TLS_method()))){
		fprintf(stderr, "SSL_CTX_new\n");
		ERR_print_errors_fp(stderr);
		goto FAILURE;
	}

	if(!SSL_CTX_load_verify_locations(ctx, chain, NULL)){
		fprintf(stderr, "SSL_CTX_load_verify_locations (chain)\n");
		ERR_print_errors_fp(stderr);
		goto FAILURE;
	}
	
	out = BIO_new_fp(stdout, BIO_NOCLOSE);
	
	if (!(bio = BIO_new_ssl_connect(ctx))){
		fprintf(stderr, "BIO_new_ssl_connect\n");
		ERR_print_errors_fp(stderr);
		goto FAILURE;
	}
	BIO_get_ssl(bio, &ssl); // return value?
	
	SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY); // returns new mode - don't really care about it yet
	
	param = SSL_get0_param(ssl);
	printf("Flags before: %s\n", x509flag2str(X509_VERIFY_PARAM_get_hostflags(param)));
	X509_VERIFY_PARAM_set_hostflags(param, X509_CHECK_FLAG_ALWAYS_CHECK_SUBJECT);
	if ((r=X509_VERIFY_PARAM_set1_host(param, servername, strlen(servername))) != 1) {
		printf("X509_VERIFY_PARAM_set1_host(%s) returned %d\n", servername, r);
		goto FAILURE;
	}
	printf("X509_VERIFY_PARAM_set1_host(%s) done\n", servername);
	printf("Flags after: %s\n", x509flag2str(X509_VERIFY_PARAM_get_hostflags(param)));
	

	BIO_set_conn_hostname(bio, connection_host_str); // always returns 1
	if(BIO_do_connect(bio) <= 0){
		fprintf(stderr, "BIO_do_connect\n");
		ERR_print_errors_fp(stderr);
		goto FAILURE;
	}
	printf("Connected to %s\n", connection_host_str);
	
	if( (r = SSL_get_verify_result(ssl)) != X509_V_OK){
		fprintf(stderr, "SSL_get_verify_result ERR: returned %s(%d)\n", x509err2str(r), r);
		ERR_print_errors_fp(stderr);
		goto FAILURE;
	}	
	fprintf(stdout, "SSL_get_verify_result SUCCESS: returned %s(%d)\n", x509err2str(r), r);

	printf("Starting...\n");
	for(;;){
		char buf[512];
		snprintf(buf, sizeof buf, "GET / HTTP/1.1\nHost: %s\n\n", servername);
		if ( (len = BIO_puts(bio, buf)) < 0){
			fprintf(stderr, "BIO_puts\n");
			ERR_print_errors_fp(stderr);
			if (!BIO_should_retry(bio)){
				printf("Not allowed to retry\n");
				goto FAILURE;
			}
		}
		if (len>0){
			printf("%d written\n", len);
			break;
		}
	}
	for(;;) {
		len = BIO_read(bio, tmpbuf, 1024);
		printf("Read %d\n", len);
		if(len <= 0){
			if(!BIO_should_retry(bio)){
				printf("should_retry returned false\n");
				break;
			}
			printf("Retrying..\n");
    }else{
			BIO_write(out, tmpbuf, len);
		}
	}
	if (len < 0){
		fprintf(stderr, "BIO_read\n");
		ERR_print_errors_fp(stderr);
		goto FAILURE;
	}

FAILURE:
	SSL_CTX_free(ctx);
	BIO_free_all(out);
	BIO_free_all(bio);
	CRYPTO_cleanup_all_ex_data();
	ERR_free_strings();
	EVP_cleanup();
// 	ERR_remove_state(0);
	return 0;
}
