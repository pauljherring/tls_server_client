//https://wiki.openssl.org/index.php/Simple_TLS_Server

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <string.h>
#include <getopt.h>

#include "tls_common.h"

static int create_socket(int port){
	int s;
	struct sockaddr_in addr;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	errno = 0;
	if ( (s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		die(EXIT_FAILURE, "unable to create socket");

	errno = 0;
	if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0)
		die(EXIT_FAILURE, "unable to bind");

	errno = 0;
	if (listen(s, 1) < 0)
		die(EXIT_FAILURE, "unable to listen");

	return s;
}

static void help(void){
	printf("\
 -c --cert <file>        public certificate file\n\
 -k --key <file>         private key file\n\
 -h --help               this help\n\
 ");
}

const char* cert, *key;
static int options(int argc, char **argv){
	int err = 0, c;
	while (1){
		static struct option long_options[] ={
			{"cert",  required_argument, 0, 'c'},
			{"key",  required_argument, 0, 'k'},
			{"help",  no_argument, 0, 'h'},
			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;
		
		if ( (c = getopt_long (argc, argv, "c:k:",
			long_options, &option_index)) == -1)
			break;
		switch (c){
			case 'c':
				cert = optarg;
				break;
			case 'k':
				key = optarg;
				break;
			case 'h':
				help();
				exit(EXIT_SUCCESS);
				break;
			default:
				err = 1;
		}
	}
	return err;
}

int main(int argc, char **argv){
	int sock;
	SSL_CTX *ctx;
	char buf[1024];
	
	if (options(argc, argv)){
		help();
		exit(EXIT_FAILURE);
	}
	
	trap_signals();
	
	SSL_load_error_strings();	// returns void
	OpenSSL_add_ssl_algorithms();	// returns void
	
	
	if (!(ctx = SSL_CTX_new(TLS_method()))) {
		ERR_print_errors_fp(stderr);
		die(EXIT_FAILURE, "Unable to create SSL context");
	}

	SSL_CTX_set_ecdh_auto(ctx, 1);

	/* Set the key and cert */
	if (SSL_CTX_use_certificate_file(ctx, cert, SSL_FILETYPE_PEM) <= 0) {
		ERR_print_errors_fp(stderr);
		die(EXIT_FAILURE,"SSL_CTX_use_certificate_file");
	}

	if (SSL_CTX_use_PrivateKey_file(ctx, key, SSL_FILETYPE_PEM) <= 0 ) {
		ERR_print_errors_fp(stderr);
		die(EXIT_FAILURE, "SSL_CTX_use_PrivateKey_file");
	}

	sock = create_socket(4433);

	/* Handle connections */
	printf("Waiting..\n");
	while(keep_running) {
		struct sockaddr_in addr;
		uint len = sizeof(addr);
		SSL *ssl;
		const char reply[] = "test\n";

		int client = accept(sock, (struct sockaddr*)&addr, &len);
		if (client < 0) {
			perror("Unable to accept");
		}else{

			printf("Got client on %d\n", client);
			ssl = SSL_new(ctx);
			SSL_set_fd(ssl, client);

			if (SSL_accept(ssl) <= 0) {
				printf("Error\n");
				ERR_print_errors_fp(stderr);
			}else {
				int i;
				memset(buf, 0, sizeof buf);
				i = SSL_read(ssl, buf, sizeof buf -1);
				printf("%d bytes read: %s\n", i, buf);
				
				i = SSL_write(ssl, reply, strlen(reply));
				printf("%d bytes written\n", i);
			}

			printf("Freeing...\n");
			SSL_free(ssl);
			printf("closing...\n");
			close(client);
			printf("Closed connection\n");
		}
	}

	close(sock);

	SSL_CTX_free(ctx);

	FIPS_mode_set(0);
	CRYPTO_set_locking_callback(NULL);
	CRYPTO_set_id_callback(NULL);
	EVP_cleanup();
	SSL_COMP_free_compression_methods();
	CRYPTO_cleanup_all_ex_data();
	ERR_free_strings();

}
