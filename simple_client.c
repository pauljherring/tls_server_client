/* OpenSSL headers */

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <string.h>


int main(int argc, char** argv){
	char tmpbuf[1024];
	int len;
	BIO * bio, *out;
	const char* message="GET / HTTP/1.1\nHost: example.com\n\n";
	SSL_load_error_strings(); // returns void
	ERR_load_BIO_strings(); // returns void
	OpenSSL_add_all_algorithms(); // returns void

	out = BIO_new_fp(stdout, BIO_NOCLOSE);
	
// 	if (!(bio = BIO_new_connect("hpdesktop.dontexist.com:4433"))){
	if (!(bio = BIO_new_connect("example.com:80"))){
		fprintf(stderr, "BIO_new_connect\n");
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
	if(BIO_do_connect(bio) <= 0) {
		fprintf(stderr, "BIO_do_connect\n");
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
	printf("Starting...\n");
// 	if ( (len = BIO_puts(bio, "GET / HTTP/1.1\nHost: example.com\n\n")) < 0){
	if ( (len = BIO_write(bio, message, strlen(message))) < 0){
		fprintf(stderr, "BIO_puts\n");
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
	for(;;) {
		len = BIO_read(bio, tmpbuf, 1024);
		printf("Read %d\n", len);
		if(len <= 0) break;
		BIO_write(out, tmpbuf, len);
	}
	if (len < 0){
		fprintf(stderr, "BIO_read\n");
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
	return 0;
}
