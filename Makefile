
# from apps/make.include
WARNING_FLAGS = \
	-Waddress \
	-Wbad-function-cast \
	-Wbuiltin-macro-redefined \
	-Wcast-align \
	-Wcast-qual \
	-Wchar-subscripts \
	-Wclobbered \
	-Wcomment \
	-Wdeclaration-after-statement \
	-Wdeprecated \
	-Wdeprecated-declarations \
	-Wdisabled-optimization \
	-Wdiv-by-zero \
	-Wdouble-promotion \
	-Wempty-body \
	-Wendif-labels \
	-Wenum-compare \
	-Wfloat-equal \
	-Wformat \
	-Wformat-contains-nul \
	-Wformat-extra-args \
	-Wformat-nonliteral \
	-Wformat-security \
	-Wformat-y2k \
	-Wformat-zero-length \
	-Wfree-nonheap-object \
	-Wignored-qualifiers \
	-Wimplicit \
	-Wimplicit-function-declaration \
	-Wimplicit-int \
	-Wimport \
	-Winit-self \
	-Winline \
	-Wint-to-pointer-cast \
	-Winvalid-memory-model \
	-Winvalid-pch \
	-Wjump-misses-init \
	-Wlogical-op \
	-Wmain \
	-Wmaybe-uninitialized \
	-Wmissing-braces \
	-Wmissing-declarations \
	-Wmissing-field-initializers \
	-Wmissing-format-attribute \
	-Wmissing-include-dirs \
	-Wmissing-parameter-type \
	-Wmissing-prototypes \
	-Wmultichar \
	-Wnarrowing \
	-Wnested-externs   \
	-Wnonnull \
	-Wold-style-declaration \
	-Wold-style-definition \
	-Woverflow \
	-Woverride-init \
	-Wpacked \
	-Wparentheses \
	-Wpedantic \
	-Wpointer-arith \
	-Wpointer-sign \
	-Wpointer-to-int-cast \
	-Wpragmas \
	-Wredundant-decls \
	-Wreturn-local-addr \
	-Wreturn-type \
	-Wsequence-point \
	-Wshadow \
	-Wsign-compare \
	-Wsizeof-pointer-memaccess \
	-Wstrict-aliasing=3 \
	-Wstrict-overflow=1 \
	-Wstrict-prototypes \
	-Wsuggest-attribute=format \
	-Wsuggest-attribute=noreturn \
	-Wswitch \
	-Wswitch-default \
	-Wswitch-enum \
	-Wtrigraphs \
	-Wtype-limits \
	-Wundef \
	-Wuninitialized \
	-Wunknown-pragmas \
	-Wunsafe-loop-optimizations \
	-Wunused \
	-Wvarargs \
	-Wvariadic-macros \
	-Wvla \
	-Wvolatile-register-var \
	-Wwrite-strings \
	-pedantic-errors  \
	-std=gnu11
EXCEPTION_FLAGS = \
	-Wno-aggregate-return \
	-Wno-c++-compat \
	-Wno-unreachable-code \
	-Wno-variadic-macros 
CFLAGS += ${WARNING_FLAGS} ${EXCEPTION_FLAGS}

CFLAGS+=-g

LDFLAGS = -L${PWD} -L/usr/local/ssl/lib -Wl,-rpath=.
LDLIBS = -lssl -lcrypto -ltls_common

# Should be 4096, but using 1024 for quickness
KEY_WIDTH=1024
# KEY_WIDTH=4096

# We really should have passwords on keys, especially the root
ROOT_PASSWORD=
#ROOT_PASSWORD=-aes256
INTERMEDIATE_PASSWORD=
#INTERMEDIATE_PASSWORD=-aes256
SERVER_PASSWORD=
#SERVER_PASSWORD=-aes256

# End server
#SERVERNAME=www.example.com
# SERVERNAME=hpdesktop.dontexist.com
SERVERNAME=$(shell hostname)
IPADDRESS=$(shell ip route get 8.8.8.8 | sed -n 's/^.*src \([0-9\.]*\).*/\1/gp')
ADDITIONALDNS=, DNS:hpdesktop.dontexist.com, DNS:www.example.com
# Wait for prompt? (-batch == no)
#BATCH=
BATCH=-batch
	
all: skel verify client_4433 simple_client server

skel: ca/index.txt ca/index.txt.attr
	touch skel

tls_common.o: tls_common.c tls_common.h
	gcc -c ${CFLAGS} -fpic tls_common.c

libtls_common.so: tls_common.o
	gcc -shared -o libtls_common.so tls_common.o

client_4433: libtls_common.so

server: libtls_common.so

verify: verify_ca verify_intermediate verify_servername
	touch verify

# https://wiki.cementhorizon.com/display/CH/Example+CA%2C+Intermediate%2C+and+Server+Certificate
# https://jamielinux.com/docs/openssl-certificate-authority/introduction.html

# Create CA. This is the ultimate root cert, should be protected in an air-blocked system
# and only ever used to create new intermediate keys.
ca/private/ca.key.pem:
	openssl genrsa ${ROOT_PASSWORD} -out ca/private/ca.key.pem ${KEY_WIDTH}

ca/index.txt: 
	touch ca/index.txt
	
ca/index.txt.attr:
	# Allow duplicate certificates without `make scrub`
	echo "unique_subject = no" > ca/index.txt.attr

ca/intermediate/index.txt: ca/intermediate/index.txt.attr
	touch ca/intermediate/index.txt
	
ca/intermediate/index.txt.attr:
	# Allow duplicate certificates without `make scrub`
	echo "unique_subject = no" > ca/intermediate/index.txt.attr

ca/certs/ca.cert.pem: ca/private/ca.key.pem  
	openssl req -config ca/openssl.cnf \
		-key ca/private/ca.key.pem \
		-new -x509 -days 7300 -sha256 -extensions v3_ca \
		-out ca/certs/ca.cert.pem \
		-subj '/C=GB/ST=England/L=Newcastle/O=Nomad/OU=R&D/CN=Nomad Digital CA root'

verify_ca: ca/certs/ca.cert.pem
	openssl x509 -noout -text -in ca/certs/ca.cert.pem
	touch verify_ca
		
# Create Intermediate keys
ca/intermediate/private/intermediate.key.pem:
	openssl genrsa ${INTERMEDIATE_PASSWORD} -out ca/intermediate/private/intermediate.key.pem ${KEY_WIDTH}

#Create signing request for that intermediate
ca/intermediate/csr/intermediate.csr.pem: ca/intermediate/private/intermediate.key.pem
	openssl req -config ca/intermediate/openssl.cnf \
	-new -sha256 \
	-key ca/intermediate/private/intermediate.key.pem \
	-out ca/intermediate/csr/intermediate.csr.pem \
	-subj '/C=GB/ST=England/L=Newcastle/O=Nomad/OU=R&D/CN=Nomad Digital intermediate'

ca/serial:
	echo "1000" > ca/serial

ca/intermediate/serial:
	echo "1000" > ca/intermediate/serial

# Sign intermediate with root
ca/intermediate/certs/intermediate.cert.pem: ca/certs/ca.cert.pem ca/intermediate/csr/intermediate.csr.pem ca/serial 
	openssl ca -config ca/openssl.cnf -extensions v3_intermediate_ca \
	${BATCH} \
	-days 3650 -notext -md sha256 \
	-in ca/intermediate/csr/intermediate.csr.pem \
	-out ca/intermediate/certs/intermediate.cert.pem

verify_intermediate: ca/intermediate/certs/intermediate.cert.pem ca/certs/ca.cert.pem
	openssl x509 -noout -text -in ca/intermediate/certs/intermediate.cert.pem
	openssl verify -CAfile ca/certs/ca.cert.pem ca/intermediate/certs/intermediate.cert.pem
	touch verify_intermediate

# Certificate chain - convenience
ca/intermediate/certs/ca-chain.cert.pem: ca/intermediate/certs/intermediate.cert.pem ca/certs/ca.cert.pem
	cat ca/intermediate/certs/intermediate.cert.pem ca/certs/ca.cert.pem > ca/intermediate/certs/ca-chain.cert.pem

# Create server key
ca/intermediate/private/${SERVERNAME}.key.pem: 
	openssl genrsa ${SERVER_PASSWORD} -out ca/intermediate/private/${SERVERNAME}.key.pem ${KEY_WIDTH}

# Signing request for server
ca/intermediate/csr/${SERVERNAME}.csr.pem: ca/intermediate/private/${SERVERNAME}.key.pem
	openssl req \
	-config ca/intermediate/openssl.cnf \
	-addext "subjectAltName = DNS:${SERVERNAME},DNS:${IPADDRESS}${ADDITIONALDNS}" \
	-key ca/intermediate/private/${SERVERNAME}.key.pem \
	-new -sha256 \
	-out ca/intermediate/csr/${SERVERNAME}.csr.pem \
	-subj '/C=GB/ST=England/L=Newcastle/O=Nomad/OU=R&D/CN=${SERVERNAME}' \



# Sign server with intermediate
ca/intermediate/certs/${SERVERNAME}.cert.pem: ca/intermediate/csr/${SERVERNAME}.csr.pem ca/intermediate/index.txt ca/intermediate/serial ca/intermediate/private/intermediate.key.pem ca/intermediate/certs/intermediate.cert.pem
	openssl ca -config ca/intermediate/openssl.cnf \
	${BATCH} \
	-extensions server_cert -days 375 -notext -md sha256 \
	-in ca/intermediate/csr/${SERVERNAME}.csr.pem \
	-out ca/intermediate/certs/${SERVERNAME}.cert.pem

	
verify_servername: ca/intermediate/certs/${SERVERNAME}.cert.pem  ca/intermediate/certs/ca-chain.cert.pem
	openssl x509 -noout -text -in ca/intermediate/certs/${SERVERNAME}.cert.pem
	openssl verify -CAfile ca/intermediate/certs/ca-chain.cert.pem ca/intermediate/certs/${SERVERNAME}.cert.pem
	touch verify_servername

verify_servername_serving:
	openssl s_client -showcerts -connect ${SERVERNAME}:4433 -CAfile ./ca/intermediate/certs/ca-chain.cert.pem
	touch verify_servername_serving
# pherring@pherring-HP-Z420-Workstation:~/src/tls_server_client$ make verify_servername_serving 
# openssl s_client -showcerts -connect hpdesktop.dontexist.com:4433 -CAfile ./ca/intermediate/certs/ca-chain.cert.pem
# CONNECTED(00000005)
# depth=2 C = GB, ST = England, L = Newcastle, O = Nomad, OU = R&D, CN = Nomad Digital CA root
# verify return:1
# depth=1 C = GB, ST = England, O = Nomad, OU = R&D, CN = Nomad Digital intermediate
# verify return:1
# depth=0 C = GB, ST = England, L = Newcastle, O = Nomad, OU = R&D, CN = hpdesktop.dontexist.com
# verify return:1
# ---
# Certificate chain
#  0 s:C = GB, ST = England, L = Newcastle, O = Nomad, OU = R&D, CN = hpdesktop.dontexist.com
#    i:C = GB, ST = England, O = Nomad, OU = R&D, CN = Nomad Digital intermediate
# -----BEGIN CERTIFICATE-----



scrub:
	rm -rf \
		verify \
		skel \
		verify \
		verify_ca \
		verify_intermediate \
		verify_servername \
		ca/serial \
		ca/serial.old \
		ca/index.txt \
		ca/index.txt.old \
		ca/index.txt.attr \
		ca/index.txt.attr.old \
		ca/intermediate/serial \
		ca/intermediate/serial.old \
		ca/intermediate/index.txt \
		ca/intermediate/index.txt.old \
		ca/intermediate/index.txt.attr \
		ca/intermediate/index.txt.attr.old \
		ca/private/ca.key.pem \
		ca/certs/ca.cert.pem \
		ca/intermediate/private/intermediate.key.pem \
		ca/intermediate/csr/intermediate.csr.pem \
		ca/intermediate/certs/intermediate.cert.pem \
		ca/intermediate/crlnumber \
		ca/intermediate/certs/ca-chain.cert.pem \
		ca/intermediate/private/${SERVERNAME}.key.pem \
		ca/intermediate/csr/${SERVERNAME}.csr.pem \
		ca/intermediate/certs/${SERVERNAME}.cert.pem \
		ca/intermediate/index.txt.attr.old \
		index.txt.attr \
		client_4433 \
		hpdesktop.dontexist.com.key \
		server \
		simple_client \
		ca/newcerts/*.pem \
		ca/private/*.pem \
		ca/certs/*.pem \
		ca/intermediate/certs/*.pem \
		ca/intermediate/newcerts/*.pem 
		
%.pp: %.c
	$(CC) $(CFLAGS) -E -c $(@:.pp=.c) > $@
	



# == Example
# Server start:
# $ ./server -c ./ca/intermediate/certs/pherring-HP-Z420-Workstation.cert.pem -k ./ca/intermediate/private/pherring-HP-Z420-Workstation.key.pem
# Waiting..
#
# Client start: $ ./client_4433  -n ./ca/intermediate/certs/ca-chain.cert.pem -t hpdesktop.dontexist.com:4433
# Flags before: unknown(00)
# X509_VERIFY_PARAM_set1_host(hpdesktop.dontexist.com) done
# Flags after: unknown(01)
# Connected to hpdesktop.dontexist.com:4433
# SSL_get_verify_result SUCCESS: returned X509_V_OK(0)
# Starting...
# 46 written
# Read 5
# test
# Read 0
# should_retry returned false
#
# Server after:
# $ ./server -c ./ca/intermediate/certs/pherring-HP-Z420-Workstation.cert.pem -k ./ca/intermediate/private/pherring-HP-Z420-Workstation.key.pem
# Waiting..
# Got client on 6
# 46 bytes read: GET / HTTP/1.1
# Host: hpdesktop.dontexist.com
# 
# 
# 5 bytes written
# Freeing...
# closing...
# Closed connection
# 
# Other testing
# 
# # Valid IP address, but not in certificate
# $ ./client_4433  -n ./ca/intermediate/certs/ca-chain.cert.pem -t 127.0.0.1:4433
# lags before: unknown(00)
# 509_VERIFY_PARAM_set1_host(127.0.0.1) done
# lags after: unknown(01)
# onnected to 127.0.0.1:4433
# SL_get_verify_result ERR: returned X509_V_ERR_HOSTNAME_MISMATCH(62)

# # Valid IP address, and  in certificate
# $ ./client_4433  -n ./ca/intermediate/certs/ca-chain.cert.pem -t 172.20.3.103:4433
# lags before: unknown(00)
# 509_VERIFY_PARAM_set1_host(172.20.3.103) done
# lags after: unknown(01)
# onnected to 172.20.3.103:4433
# SL_get_verify_result SUCCESS: returned X509_V_OK(0)
# tarting...
# 5 written
# ead 5
# est
# ead 0
# hould_retry returned false


# Invalid IP address (no server there):
# $ ./client_4433  -n ./ca/intermediate/certs/ca-chain.cert.pem -t 172.20.3.102:4433
# lags before: unknown(00)
# 509_VERIFY_PARAM_set1_host(172.20.3.102) done
# lags after: unknown(01)
# IO_do_connect
# 40044266166080:error:02002071:system library:connect:No route to host:../crypto/bio/b_sock2.c:110:
# 40044266166080:error:2008A067:BIO routines:BIO_connect:connect error:../crypto/bio/b_sock2.c:111:
# 40044266166080:error:02002071:system library:connect:No route to host:../crypto/bio/bss_conn.c:173:hostname=172.20.3.102 service=4433
# 40044266166080:error:20073067:BIO routines:conn_state:connect error:../crypto/bio/bss_conn.c:177:
# 