# Example usage

Start server

```
$ ./server -c ./ca/intermediate/certs/`hostname`.cert.pem -k ./ca/intermediate/private/`hostname`.key.pem
Waiting..
```


Run client against a few hosts that resolve locally, some in, some not in the cert:


Good using `hostname`:
```
$ ./client_4433 -n ./ca/intermediate/certs/ca-chain.cert.pem -t `hostname`:4433
Flags before: unknown(00)
X509_VERIFY_PARAM_set1_host(pherring-HP-Z420-Workstation) done
Flags after: unknown(01)
Connected to pherring-HP-Z420-Workstation:4433
SSL_get_verify_result SUCCESS: returned X509_V_OK(0)
Starting...
51 written
Read 5
test
Read 0
should_retry returned false
```

Good using hpdesktop.dontexist.com:
```
$ ./client_4433 -n ./ca/intermediate/certs/ca-chain.cert.pem -t hpdesktop.dontexist.com:4433
Flags before: unknown(00)
X509_VERIFY_PARAM_set1_host(hpdesktop.dontexist.com) done
Flags after: unknown(01)
Connected to hpdesktop.dontexist.com:4433
SSL_get_verify_result SUCCESS: returned X509_V_OK(0)
Starting...
46 written
Read 5
test
Read 0
should_retry returned false
```

Bad using localhost (since 'localhost' isn't in the cert)
```
$ ./client_4433 -n ./ca/intermediate/certs/ca-chain.cert.pem -t localhost:4433
Flags before: unknown(00)
X509_VERIFY_PARAM_set1_host(localhost) done
Flags after: unknown(01)
Connected to localhost:4433
SSL_get_verify_result ERR: returned X509_V_ERR_HOSTNAME_MISMATCH(62)
```
