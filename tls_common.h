#ifndef tls_common_h__
#define tls_common_h__

const char* x509err2str(int i);
const char* x509flag2str(int i);

extern int keep_running;
void s_signal_exit(int signal_value);
void s_signal_ignore(int signal_value);
void trap_signals(void);
void die(int retval, const char* error);

#endif // tls_common_h__
